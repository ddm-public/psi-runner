# PSI Runner

Allows adding PageSpeed Insights to your build process, which are then emailed to a target email address

## Usage
Specify the following environment variables, e.g. in a `.env` file
```
# All variables are required
#
# KEY                Get a key from https://developers.google.com/speed/docs/insights/v5/get-started
# STRATEGY           "mobile" or "desktop"
# ROOT_URL           Root URL, including protocol (e.g. "https://www.example.com")
# SUB_URLS           Various suburls, separated by a space. These will be appended to the ROOT_URL 
#                         e.g. "foo bar baz" will become https://www.example.com/foo, 
#                         https://www.example.com/bar, etc.
#
# Mail Options:
# MAILTO            Destination Mailing Address to send results
# MAILFROM          Source Mailing Address
#
# Various SMTP Options
# SMTP_USERNAME
# SMTP_PASSWORD
# SMTP_SERVER
# SMTP_PORT
```

NOTE: `SMTP_PORT` must refer to an SSL port; TLS will cause nodemailer to throw an error

Then call the Docker Container, e.g. `docker run --env-file .env registry.gitlab.com/ddm-public/psi-runner:latest`