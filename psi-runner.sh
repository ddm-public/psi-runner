# Expected Environment Variables

# KEY                Get a key from https://developers.google.com/speed/docs/insights/v5/get-started
# STRATEGY           "mobile" or "desktop"
# ROOT_URL           Root URL, including protocol (e.g. "https://www.example.com")
# SUB_URLS           Various suburls, separated by a space. These will be appended to the ROOT_URL 
#                         e.g. "foo bar baz" will become https://www.example.com/foo, 
#                         https://www.example.com/bar, etc.
#
# Mail Options:
# MAILTO            Destination Mailing Address to send results
# MAILFROM          Source Mailing Address
#
# Various SMTP Options
# SMTP_USERNAME
# SMTP_PASSWORD
# SMTP_SERVER
# SMTP_PORT

echo "Getting score for $ROOT_URL"
touch output.txt
psi --key "$KEY" --format cli --strategy "$STRATEGY" "$ROOT_URL" 2>&1 | tee output.txt

echo >> output.txt
echo "---------------" >> output.txt
echo "Sub URLs" >> output.txt
echo "---------------" >> output.txt
for url in $SUB_URLS
do
    full_url="$ROOT_URL/$url"
    echo -n "URL: $full_url || " | tee -a output.txt
    psi --key "$KEY" --format cli --strategy "$STRATEGY" "$full_url" 2>/dev/null | grep "Performance" | tee -a output.txt
done

nodemailer -j "PageSpeed Insights at $(date) on $ROOT_URL" --body output.txt $MAILTO $MAILFROM
